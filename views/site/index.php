<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use coderius\swiperslider\SwiperSlider;
use yii\helpers\Url;

$this->title = 'Banesto Biking';
?>

<div class="site-index">
    
    <div class="row bigseparator">
        
        <?= SwiperSlider::widget([
            'slides' => [
            Html::img('@web/images/slider/slider1.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
            Html::img('@web/images/slider/slider2.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
            Html::img('@web/images/slider/slider3.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
                ],
            'options' => [
                'styles' => [
                    SwiperSlider::CONTAINER => ["height" => "auto", "width" => "100%"],
                    SwiperSlider::SLIDE => ["text-align" => "center"],
                    ],
                ],
            ]);

        ?>
        
    </div>
    
    <hr class="bigseparator">
    
    <div class="card">
        <div class="text-center coloresletrablanca card-body bg-banesto" style="margin-top: -10px">
            <h1 class="display-4 ">Ultimas noticias</h1>
            <br>
            <div class="row">

                <div class="col-md-4">
                    <h3>Alex Zulle se enfunda el primer jersey de oro</h3>
                    <p class="zoom"><?= Html::img('@web/images/noticias/noticia1.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imgnoticias'])?> <p/>
                    <hr class="separator">
                    <p>Ganaba la etapa el suizo Alex Zulle (Banesto) que marcaba un crono de 17.09, en un recorrido de 13,3 kilómetros muy difícil. El suizo le ha permitido respirar a Banesto; Abraham Olano se ha metido delante, con los mejores, que es la lectura más positiva que puede hacer el líder de la Once que parece salir de forma definitiva del pozo en el que se metió en el Tour de Francia.
                    </p>
                    <p><?= Html::a('Leer mas',[''],['class'=>'btn btn-info'])?></p>
                </div>

                <div class="col-md-4">
                    <h3>Bossoni gana en Valencia y Escartín vuelve a ceder</h3>
                    <p class="zoom"><?= Html::img('@web/images/noticias/noticia2.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imgnoticias'])?> <p/>
                    <hr class="separator">
                    <p>Aunque todo hacía presagiar que sería una jornada tranquila, lo cierto es que desde el principio se produjeron escaramuzas en el seno del pelotón. Los intentos de escapada se repetían, pero ninguno de ellos consiguió abrir un hueco importante. Al acercarse a Valencia, el viento hizo su aparición y una multitudinaria caída disgregó el pelotón.
                    </p>
                    <br>
                    <p><?= Html::a('Leer mas',[''],['class'=>'btn btn-info'])?></p>
                </div>

                <div class="col-md-4">
                    <h3>Eladio Jiménez estrena su palmarés en Xorret de Catí</h3>
                    <p class="zoom"><?= Html::img('@web/images/noticias/noticia3.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imgnoticias'])?> <p/>
                    <hr class="separator">
                    <p>Jiménez se llevó una etapa en la que el italiano Fabio Roscioli (Jazztel-Costa Almería) estuvo escapado muchos kilómetros, pero terminó clavado en las rampas del puerto de primera categoría. El líder Alex Zulle (Banesto) demostró una vez más su buen estado de forma y sólo cedió unos segundos frente al ganador y Roberto Heras, segundo en la línea de meta. 
                    </p>
                    <p><?= Html::a('Leer mas',[''],['class'=>'btn btn-info'])?></p>
                </div>

            </div>
        </div>
    </div>
    
    <hr class="bigseparator">
    
    <div class=" text-left bg-transparent">
        <h1 class="display-4 coloresletrablanca">Hoy hablamos de de Miguel Indurain</h1>
        <hr>
        <div class="row">
            <div class="container col-md-4"> 
                <?= Html::img('@web/images/indurain/indurain1.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imgrect'])?>
            </div>
            <!-- Texto a la derecha de slider de indurain-->
            <div class=" col-md-8 coloresletrablanca titulo" style=" text-align: left">
                <p> "El cuerpo aguanta más que la mente" cita textual del ciclista español considerado uno de los mejores del mundo. Hablamos de la leyenda del deporte Miguel Induráin, de 57 años de edad, que fue ganador de cinco Tours de Francia (1991-1995) y del Giro de Italia por dos años consecutivos (1992 y 1993), además de Campeón del mundo contrarreloj (1995) y poseedor del récord de la hora durante dos meses (1994).
                    Además de sus múltiples victorias en el Tour de Francia, Miguel Induráin ganó varias vueltas por etapas de una semana y clásicas de un día, destacando más la Volta a Cataluña, la París-Niza, la Clásica de San Sebastián, el Campeonato de España en Ruta y la Dauphiné Libére. ¡Sigue leyendo y descubre más sobre la vida y biografía de Miguel Induráin, una leyenda eterna del ciclismo español!
                </p>
            </div>
        </div>

    </div> 
    <hr class="bigseparator">
</div>
<!-- esto es un simulacro -->