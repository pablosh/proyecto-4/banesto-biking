<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use coderius\swiperslider\SwiperSlider;

$this->title = '¿Quienes somos?';
//$this->params['breadcrumbs'][] = $this->title;
?>

<?= SwiperSlider::widget([
    'slides' => [
    Html::img('@web/images/slider/about1.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
    Html::img('@web/images/slider/about2.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
    Html::img('@web/images/slider/about3.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imagen']),
        ],
    'options' => [
        'styles' => [
            SwiperSlider::CONTAINER => ["height" => "450px", "width" => "98%"],
            SwiperSlider::SLIDE => ["text-align" => "center"],
            ],
        ],
    ]);

?>

<hr class="bigseparator" style="width: 0%">

<div class="row">
    <div class= "site-about coloresletrablanca col-md-6 titulo">
        <h1><?= Html::encode($this->title) ?></h1>
        <br>

        <p>
            BanestoTeam es el equipo ciclista más longevo y exitoso en cuanto a trayectoria del UCI WorldTour, la máxima categoría del ciclismo internacional, que engloba a las 18 mejores escuadras masculinas del mundo.<p>
            En 2003, el equipo masculino alcanza su 23ª temporada ininterrumpida en la elite, con 29 corredores y un amplio staff técnico completa una estructura cercana al centenar de empleados totales.</p>
        A lo largo de sus más de cuatro décadas de trayectoria, el equipo solo ha tenido 2 sponsors principales: Reynolds (aluminios, 1980-89), Banesto (banco, 1990-2003).<p>
            Además de sus más de 500 victorias como estructura masculina profesional con sede en Europa -así como medio centenar como conjunto femenino-, el equipo ha acabado 1º en el ranking mundial por equipos en una ocasión: 1992. </p>
        </p>

    </div>

    <div class= "site-about col-md-6" style="text-align: center">
        <?= Html::img('@web/images/indurain/indurain3.jpg', ['alt' => 'Imagen no encontrada', 'class' => 'imgrect', 'style' => 'align: center'])?>
    </div>
    <hr class="bigseparator" style="width: 0%">
</div>
<hr class="bigseparator" style="width: 0%">