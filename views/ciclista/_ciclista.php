<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$clave = $model->dorsal;
?>
<!-- <?= var_dump($model) ?> -->
<div class="card centrar separator zoom">
    <div class="card-body text-white bg-banesto">
        
        <div class="caption coloresletrablanca">
            <h2><?= $model->dorsal ?></h2>
            <h4><?= $model->nombre ?></h4>
            <?php echo Html::img('@web/images/ciclistas/ciclista'.$clave.'.jpg', [
            'alt' => 'Imagen no encontrada', 'class' => 'imagen'
            ]) ?>
            <hr class="my4">
            <h5><?= $model->edad?> años</h5>
            <!-- Mostrar contador de etapas y puertos ganados -->
            <p>Etapas: <?= $model->ets?> - Puertos:  <?= $model->puer?></p>
            <p>Maillots: <?= $model->maillts?></p>
        </div>

    </div>
</div> 