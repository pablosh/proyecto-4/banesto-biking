<?php
use yii\helpers\Html;
use yii\helpers\HTMLPurifier;
use circulon\widgets\ColumnListView;
$titulo = "Ciclistas de nuestro equipo";
$this->title = $titulo;
?>

<div class="well well-sm separator coloresletrablanca"><h2 style="text-align: center; max-height: 80px"><?=$titulo?></h2></div>

<div class="coloresletrablanca">
    <?= ColumnListView::widget([ //Mostrar un los ciclistas en bloques de 4
        'dataProvider' => $dataProvider,
        'itemView' => '_ciclista',
        'layout'=> "\n{pager}\n{items}",
        'columns' => 2,
    ]);
    ?>
</div>